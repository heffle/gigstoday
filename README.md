# gigstoday

## About the project

This is a simple python application that scrapes gigs information from [rockgig](https://rockgig.net) and prints it out in an easy readable-way.

## Built With

Application was built using bs4, tabulate and random_user_agent pip packages.

## Prerequisites

No prerequisites needed.

## Installation

1. Clone a repo
`git clone https://gitlab.com/heffle/gigstoday`

2. Install requirements
`pip install -r requirements.txt`

## Docker 

You can use this app with Docker.

`docker-compose up`

## Usage

By default the app prints out the gigs for today. You can specify a date by passing an argument in 'year-month-day' format. Site also can block you for some time (around a day) if you send too many requests.

## TODO

- [ ] Develop a telegram bot
- [ ] Provision infrastracutre with a caching mechanism and a trigger with pulling info about gigs every day.
- [ ] Add a mechanism to prevent server blocking from too many requests.

## License 

Distributed under the MIT License.

## Contact

mail - nazarovilia.it@gmail.com

telegram @archesky1
