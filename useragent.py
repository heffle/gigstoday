from random_user_agent.user_agent import UserAgent
from random_user_agent.params import SoftwareName, OperatingSystem

software_names = [SoftwareName.CHROME.value]
operating_systems = [OperatingSystem.WINDOWS.value, OperatingSystem.LINUX.value]   

user_agent_rotator = UserAgent(software_names=software_names, operating_systems=operating_systems, limit=100)

def random_user_agent():
    # Get list of user agents.
    user_agents = user_agent_rotator.get_user_agents()
    # Get Random User Agent String.
    user_agent = user_agent_rotator.get_random_user_agent()
    return user_agent