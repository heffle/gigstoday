import requests
from bs4 import BeautifulSoup
from useragent import *
from tabulate import tabulate
import sys

user_agent_header = {'User-Agent': random_user_agent()}

url = "https://rockgig.net/schedule/today"


#You can specify specific date with 'year-month-day' format
if len(sys.argv) > 1:
	url = "https://rockgig.net/schedule/" + sys.argv[1]
	

# Send an HTTP GET request to the website
response = requests.get(url, headers=user_agent_header)

if str(response.status_code) != '200':
	print ('Responce code is ' + str(response.status_code) + '. Something is wrong.. Exiting...')
	exit()

#Parse the HTML code using BeautifulSoup
soup = BeautifulSoup(response.content, 'html.parser')

#Extract the relevant information from the HTML code

gigs = []

for row in soup.find_all("div", attrs={"class": "el BG0"}):
	git_time = ''
	gig_price = '0'
	author = row.find("div", attrs={"ob elBand"}).get_text()
	if author == '':
		author = row.find("div", class_="elName").get_text()
	link = row.find("div", attrs={"elName"}).find('a')['href']
	genre = row.find("div", attrs={"elGenr"}).get_text()
	try:
		time_price = row.find("div", attrs={"ob iTime eD"}).get_text().replace("\xa0 ₽\u2009", "", 1).replace('+',"")
		gig_time = time_price.split(' ', 1)[0]
		gig_price = time_price.split(' ', 1)[1].strip()
	except:
		pass
	gigs.append([author, genre, gig_time, gig_price])


sorted_by_time = sorted(gigs, key=lambda tup: tup[2])

table_by_time = tabulate(sorted_by_time, headers=['Исполнитель', 'Жанр', 'Время', 'Цена'], tablefmt="github")
