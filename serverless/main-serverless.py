import requests
from bs4 import BeautifulSoup
from useragent import *
import sys

def handler(event, context):
	user_agent_header = {'User-Agent': random_user_agent()}
	
	url = "https://rockgig.net/schedule/today"
	#You can specify specific date with 'year-month-day' format
	if len(sys.argv) > 1:
		url = "https://rockgig.net/schedule/" + sys.argv[1]
		
	user_agent_header = {"User-Agent":"Mozilla/5.0 (Linux; U; Linux i652 x86_64) AppleWebKit/536.14 (KHTML, like Gecko) Chrome/53.0.3177.211 Safari/601"}

	response = requests.get(url, headers=user_agent_header)

	if str(response.status_code) != '200':
		return('Responce code is ' + str(response.status_code) + '. Something is wrong.. Exiting...')

	#Parse the HTML code using BeautifulSoup
	soup = BeautifulSoup(response.content, 'html.parser')

	#Extract the relevant information from the HTML code

	gigs = []
	for row in soup.find_all("div", attrs={"class": "el BG0"}):
		git_time = 'Неизвестно'
		gig_price = 'Неизвестно'
		author = row.find("div", attrs={"ob elBand"}).get_text()
		if author == '':
			author = row.find("div", class_="elName").get_text()
		link = row.find("div", attrs={"elName"}).find('a')['href']
		genre = row.find("div", attrs={"elGenr"}).get_text()
		try:
			time_price = row.find("div", attrs={"ob iTime eD"}).get_text().replace("\xa0 ₽\u2009", "", 1).replace('+',"")
			gig_time = time_price.split(' ', 1)[0]
			gig_price = time_price.split(' ', 1)[1].strip() + 'р.'
		except:
			pass
		gigs.append([author, genre, gig_time, gig_price, link])

	sorted_by_time = sorted(gigs, key=lambda tup: tup[2])
	
	converted = ''
	index = 0
	for item in sorted_by_time:
		index += 1
		converted += str(index) + '. ' + str(item[0]) + '.' + str(item[1]) + ' Жанр: ' + str(item[2]) + ' Время: ' + str(item[3]) + ' Цена: ' + str(item[4]) + 'р. \n ' 

	return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'text/plain; charset=utf-8'
        },
        'isBase64Encoded': False,
        'body': converted
    }


