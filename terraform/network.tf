resource "yandex_vpc_network" "gigs-network" {
  name      = "gigs-network"
  folder_id = yandex_resourcemanager_folder.gigstoday.id
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.gigs-network.id
  v4_cidr_blocks = ["192.168.1.0/24"]
  folder_id      = yandex_resourcemanager_folder.gigstoday.id
}
