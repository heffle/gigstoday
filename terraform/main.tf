#Create a separate folder for a project
resource "yandex_resourcemanager_folder" "gigstoday" {
  name = "gigstoday"
}

#Create a separate SA for project
resource "yandex_iam_service_account" "gigstoday-function-sa" {
  name        = "gigstoday-function-sa"
  description = "service account to invoke functions"
  folder_id   = yandex_resourcemanager_folder.gigstoday.id
}

#Create a separate SA for project
resource "yandex_iam_service_account" "gigstoday-admin-sa" {
  name        = "gigstoday-admin-sa"
  description = "service account to manage gigstoday folder"
  folder_id   = yandex_resourcemanager_folder.gigstoday.id
}


resource "yandex_resourcemanager_folder_iam_member" "function-invoke-account-iam" {
  folder_id = yandex_resourcemanager_folder.gigstoday.id
  role      = "functions.functionInvoker"
  member    = "serviceAccount:${yandex_iam_service_account.gigstoday-function-sa.id}"
}


resource "yandex_resourcemanager_folder_iam_member" "gigtoday-admin-account-iam" {
  folder_id = yandex_resourcemanager_folder.gigstoday.id
  role      = "admin"
  member    = "serviceAccount:${yandex_iam_service_account.gigstoday-admin-sa.id}"
}
