
// Use keys to create bucket
resource "yandex_storage_bucket" "gigstoday" {
  bucket     = "gigstoday-bucket"
  acl        = "public-read"
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
}

// Create Static Access Keys
resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = yandex_iam_service_account.gigstoday-admin-sa.id
  description        = "static access key for object storage"
}

resource "yandex_storage_object" "source_code" {
  bucket     = "gigstoday-bucket"
  key        = "gigstoday.zip"
  source     = "../gigstoday.zip"
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
}


