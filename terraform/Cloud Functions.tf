resource "yandex_function" "gigs_today" {
  name        = "gigs"
  description = "Print out gigs in Moscow"
  folder_id   = yandex_resourcemanager_folder.gigstoday.id
  user_hash   = "first-function"
  runtime     = "python37"
  package {
    bucket_name = "gigstoday-bucket"
    object_name = "gigstoday.zip"
  }

  entrypoint        = "main-serverless.handler"
  memory            = "128"
  execution_timeout = "10"
  #gigure this out
  service_account_id = yandex_iam_service_account.gigstoday-admin-sa.id
}

output "yandex_function_gigs" {
  value = yandex_function.gigs_today.id
}
