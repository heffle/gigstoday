resource "yandex_function_trigger" "gigs-trigger" {
  name        = "scrape-gigs-daily"
  description = "Daily scrape gigs from https://rockgig.net/schedule/today and store in a bucket"
  folder_id   = yandex_resourcemanager_folder.gigstoday.id

  timer {
    #Daily in 01-00 moscow (UTC-0 time on cron)
    cron_expression = "0 22 ? * * *"
  }

  function {
    id                 = yandex_function.gigs_today.id
    service_account_id = yandex_iam_service_account.gigstoday-admin-sa.id
  }
}
