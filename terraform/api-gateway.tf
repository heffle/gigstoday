resource "yandex_api_gateway" "api-gateway" {
  name        = "api-gateway"
  description = "API-gateway for telegram bot and serverless functions"
  folder_id   = yandex_resourcemanager_folder.gigstoday.id
  spec        = <<-EOT
openapi: "3.0.0"
info:
  version: 1.0.0
  title: for-python-tg-bot
paths:
  /:
    post:
     x-yc-apigateway-integration:
       type: cloud-functions
       function_id: ${yandex_function.gigs_today.id}
     operationId: tg-webhook-function
  
EOT
}
